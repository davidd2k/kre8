#!/bin/zsh -e

# For those who use ferm for simpler/safer iptables management

# adjust if/as needed -- for archive.org this limits port incoming packets to inside internal cluster
SADDR='saddr $CLUSTER'


function main() {
  # attempts to open up needed TCP/UDP ports in our firewall
  if [ ! -e /etc/ferm ]; then
    echo "


YOUR SETUP DOESNT SEEM TO BE USING FERM.


That's OK - but you MAY need to unfirewall certain ports:
   443 - open to world for https traffic to your 'review apps' via ingress
  6443 - open to world for laptop 'kubectl' access


View the source of this script for additional ports that might need opening.


"
    return
  fi


  set -x

  ( echo '# see  https://gitlab.com/internetarchive/kre8/ports-unblock.sh'
    # TCP -- start with documented list of ports needed:
    # https://coreos.com/kubernetes/docs/latest/kubernetes-networking.html
    open_port_to_world  tcp    443 # API server - https into ingress
    open_port_to_world  tcp   6443 # master node - so laptops can talk to cluster via kubectl

    open_port           tcp  10250 # master node
    open_port           tcp  10255 # heapster
    open_port           tcp   2379 # master nodes 	etcd server client API - DONT open to world!
    open_port           tcp   2380 # master nodes 	etcd server client API - DONT open to world!

    open_port           tcp   9090 # prometheus
    open_port           udp   6782 # prometheus metrics port

    # helm uses tunnel dynamic ports (so wide range - but $SADDR only)
    open_port           tcp  '30000:45000'

    # UDP needed for DNS
    open_port           udp     53 # DNS
    open_port           udp  10053 # DNS
    open_port           udp   6783 # weave data port
    open_port           udp   6784 # weave data port

    # undocumented ports:
    open_port           tcp   6783 # weave DNS control port https://www.weave.works/docs/net/latest/faq/
    open_port           tcp   8081 # kube DNS and/or weave
    open_port           tcp  10053 # kube DNS and/or weave
    open_port           tcp  10054 # kube DNS and/or weave
    open_port           tcp  10055 # kube DNS and/or weave
    open_port           tcp  10251 # kube scheduler  healthz
    open_port           tcp  10252 # kube controller healthz

    # https://coreos.com/flannel/docs/latest/troubleshooting.html#firewalls
    open_port           udp   8472 # flannel port (we are using 'vxlan' backend)


    echo 'proto icmp icmp-type echo-request ACCEPT;'
  ) |sudo tee /etc/ferm/input/kubernetes.conf

  sudo cp -p  /etc/ferm/input/kubernetes.conf  /etc/ferm/forward/
  sudo cp -p  /etc/ferm/input/kubernetes.conf  /etc/ferm/output/

  # avoid explicit reject in the forward chain (@dvd is our expert here):
  echo 'ACCEPT;' | sudo tee /etc/ferm/forward/docker.conf

  set +e
  sudo service ferm reload
  set -e
}

function open_port()          { open_porter $1 $2 "${SADDR?}"; }
function open_port_to_world() { open_porter $1 $2              ; }

function open_porter() {
  PROTO="$1"
  PORT="$2"
  SADDR_ARG="$3"
  echo "$SADDR_ARG proto $PROTO dport $PORT ACCEPT;"
}

main
