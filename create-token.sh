#!/bin/zsh -e

# Creates and/or prints the 'Token' for the GitLab 'Kubernetes cluster details' settings.
#
# tracey & dvd developed from:
#   https://docs.gitlab.com/ee/user/project/clusters/#adding-an-existing-kubernetes-cluster


kubectl create -nkube-system serviceaccount gitlab-admin  ||  (
  kubectl get -nkube-system sa/gitlab-admin
  echo
  echo OK - serviceaccount looks like was already created
  echo
)

kubectl create clusterrolebinding gitlab-admin --clusterrole=cluster-admin --serviceaccount=kube-system:gitlab-admin  ||  (
  echo 'likely already created... proceeding and will succeed/fail next'
)


# Print the token for the gitlab-admin service account
echo
kubectl get -nkube-system secret/$(kubectl get -nkube-system secret |egrep ^gitlab-admin |cut -f1 -d' ') -o jsonpath="{['data']['token']}" |base64 --decode
echo
