#!/bin/zsh -e

# NOTE: presently not working -- the `containerd.sock` not compatible with `docker.sock`??
#
# Changes k8 (cluster-wide) GitLab runners from "docker-in-docker" to use docker socket
#
# It's a PITA since it's a shell script embedded in a config map (ultimately) that we need to update.
# Gist:  https://gitlab.com/gitlab-org/gitlab-runner/issues/2578

mydir=${0:a:h}
source $mydir/aliases
source $mydir/opts

# should be right -- but can make SOCK_SRC=$SOCK_DST in case for some reason your cluster is running
# `docker` not k3s `crictl` (k3s' default)
SOCK_DST=/var/run/docker.sock
SOCK_SRC=/run/k3s/containerd/containerd.sock

# setup cmd-line options
typeset -A o
o[NS]="gitlab-managed-apps"
o[CM]="runner-gitlab-runner"

process-args "$0" "$@"
print-args "$0"

line(){ perl -e 'print "_"x80; print "\n\n";'; }

set -x

# get/show current config map
kc get -n $o[NS] cm/$o[CM] -o yaml |tee now.yml
line


# get/show current (just the) CM entrypoint
kc get -n $o[NS] cm/$o[CM] -o jsonpath='{.data.entrypoint}' |tee entrypoint.yml
line


# patch it (courage, it's a long game)
SRC=https://gitlab.com/internetarchive/kre8/blob/master/use-docker-socket-not-dind.sh
TOML=/etc/gitlab-runner/config.toml
TOML=/home/gitlab-runner/.gitlab-runner/config.toml

# NOTE: multi-line / slightly strange to ensure lead whitespace is exact
# we'll make new dest: `/scripts/docker-socket` file with the contents below
# (which is actually a bash script that modifies (appends to) `$TOML` file..)
kc patch -n $o[NS] cm/$o[CM] --type strategic --patch "
data:
  docker-socket: |
    echo ''                                               >> $TOML
    echo '      # From $SRC'                              >> $TOML
    echo '      [[runners.kubernetes.volumes.host_path]]' >> $TOML
    echo '        name = \"whats-up-dock\"'               >> $TOML
    echo '        host_path  = \"$SOCK_SRC\"'             >> $TOML
    echo '        mount_path = \"$SOCK_DST\"'             >> $TOML
    echo '        read_only = true'                       >> $TOML
"
line


# now slip our changes above into a modified (local to laptop) entrypoint patch file
( echo '
data:
  entrypoint: |'
  cat entrypoint.yml |perl -pe 's/^/    /'
  echo
) |perl -pe 's=(exec /entrypoint run)=bash /scripts/docker-socket; $1=' |tee entrypoint-patch.yml
line

# actually patch the configmap
kc patch -n $o[NS] cm/$o[CM] --type strategic --patch "$(cat entrypoint-patch.yml)"


# show updated config map
sleep 30
kc get -n $o[NS] cm/$o[CM] -o yaml |tee new.yml
line


# restart the runner main pod
POD=$(kc get -n $o[NS] pod |egrep ^runner-gitlab-runner- |cut -f1 -d ' ')
kc delete -n $o[NS] pod/$POD
sleep 5
line

# now print/check the updated live runner config
POD=$(kc get -n $o[NS] pod |egrep ^runner-gitlab-runner- |cut -f1 -d ' ')
kc exec -it -n $o[NS] $POD cat /home/gitlab-runner/.gitlab-runner/config.toml |tee updated.yml
line

echo new POD $POD
