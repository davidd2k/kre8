#!/bin/zsh -e

# Script to try to find kubernetes resources that may still be around and axe them.
# Probably `| tee FILE` the stdout and then `bash -x FILE` once you've manually reviewed it.
#
# Run this when you are `cd`-ed into the repo of interest.
#
# Example?  when a branch is deleted, its 'review app' related resources are supposed to go away.
# In our first few weeks of use, tracey found 3 such branches with resources still around.

mydir=${0:a:h}
source $mydir/aliases


function main() {
  delete-resources-for-deleted-branches
  delete-obsoleted-replicaset
  delete-unused-pvc
  delete-unused-ingressroute
  echo '# SUCCESS'
}


function delete-resources-for-deleted-branches() {
  # Tries to remove 'review app' (etc.) resources still around for branches that have been deleted.

  rm -rf .nix
  mkdir  .nix

  # ensure we have all branches in local git repo
  git pull |perl -ne 'print "# "; print'

  # remove any branches that have been deleted/merged
  git remote prune origin |perl -ne 'print "# "; print'

  # list all branches from origin's POV
  git branch -a \
    | fgrep remotes/origin \
    | fgrep -v remotes/origin/HEAD \
    | perl -pe 's=^\s*remotes/origin/==' \
    | sort -u -o .nix/gitbranches

  # NOTE: OK _some_ chars in branch names get 'slugged' here -- these are _registry_ names
  # _in reality_ that have been used as the closest branch proxy
  SLUG=$(k8-slug)
  pods |egrep "^${SLUG}-"    |cat >  .nix/pods.tsv
  cat .nix/pods.tsv |cut -f2 |killspace |sort -u -o .nix/deployed

  cat .nix/gitbranches |branch-to-registry-slug |sort -u -o .nix/gitbranches-slugged

  echo -n >| .nix/delete-k8-app
  anotb .nix/deployed .nix/gitbranches-slugged |egrep -v "^${SLUG}$" |sort -u -o .nix/delete-registry-slugs

  # NOTE: specific order so we destroy the resources in that order
  ( kc get deploy,svc,rs,pod,ingress,cm,secret,endpoints,pv,pvc |fgrep "/${SLUG}-" |cat
  ) >| .nix/resources


  IFS=$'\n' # NEWLINE / ENTER / RETURN splitting, not SPACE
  for BRANCH_SLUG in $(cat .nix/delete-registry-slugs); do
    echo '#############################################################################'
    # examples:
    #   branch: update/IAUX
    #   BRANCH_SLUG=update-iaux
    #   APP=ia-petabox-update-iaux
    APP=$(grep -E "\t$BRANCH_SLUG\$" .nix/pods.tsv |cut -f2)
    echo $APP >> .nix/delete-k8-app
    echo "# KILLING RESOURCES FOR GONE BRANCH SLUG $BRANCH_SLUG => $APP"
    TRUNC=$(echo "$APP" |cut -b1-53)
    for LINE in $(fgrep /$TRUNC .nix/resources |tr -s ' ' |cut -f1 -d' '); do
      echo kubectl delete $LINE \;
    done
  done
}


function delete-obsoleted-replicaset() {
  # finds replicasets where DESIRED=0 CURRENT=0 READY=0 and axes them

  # iterate over list of namespaces w/ deployments
  for NS in $(kc get -A deploy --no-headers --selector=tier=web -o=custom-columns=NAME:.metadata.namespace |sort -u); do
    for RS in $(kc get -n${NS?} rs |fgrep '0         0         0' |cut -f1 -d' '); do
      echo "# REMOVING NOT-NEEDED REPLICASET $RS"
      echo kubectl delete -n${NS?} replicaset/$RS \;
    done
  done
}


function delete-unused-pvc() {
  kc describe -A pod |fgrep ' ClaimName: ' |cut -f2 -d: |killspace |sort -u -o .nix/pvc-pods
  kc get pvc --no-headers -o=custom-columns=NAME:.metadata.name    |sort -u -o .nix/pvc-alive
  for PVC in $(anotb .nix/pvc-alive .nix/pvc-pods); do
    echo "kubectl delete pvc $PVC \;"
    echo "kubectl delete pv  $PVC \;"
  done
}


function delete-unused-ingressroute() {
  # traefik LB *v2* requires us to insert ingressroute resources in for each deploy.
  # remove any routes for gone ingress resources.
  ( set +e; kc get ingressroute --no-headers -o=custom-columns=NAME:.metadata.name |sort -u -o .nix/rte )
  ( set -e; kc get ingress      --no-headers -o=custom-columns=NAME:.metadata.name |sort -u -o .nix/ing )
  for ROUTE in $(anotb .nix/rte .nix/ing); do
    echo kubectl delete ingressroute $ROUTE \;
  done
}


function branch-to-registry-slug() {
  # GitLab makes registry image names based on slugged branch names -- and uniques repeated `-` chars.
  # (It also truncates at 53 chars.  Welcome to astronaut training).
  SLUG=$(k8-slug)
  perl -ne 'chop; $slug=lc $_; $slug=~s/[^0-9a-z-]/-/g; $slug=~s/\-+/-/g; print "'$SLUG'-".substr($slug,0,53-length("'$SLUG'-"))."\t$_\n"'
}


function anotb () {
  # limited diff of 2 sorted files - where we report lines in file 'A' not in file 'B'
	sort -c "$1"  &&  sort -c "$2"  &&  join -v 1 "$1" "$2"
}


function killspace () {
  # collapse whitespace to single [SPACE] chars and trim lead/trail whitespace
	perl -ne 's/\s*(\n)//; $ f = $ 1; s/^\s+//; s/\s+/ /g;  next unless m/./; print; print $ f ;'
}



main
